from ims_base.apis import BaseAPIViewSet
from ims_user.apis import UserFilter
from rest_framework.filters import BaseFilterBackend
from rest_framework.generics import RetrieveAPIView

from ims_certificate.models import Certificate
from ims_certificate.serializers import CertificatePublicSerializer


class CertificateRegistrationFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "registration_id" in request.query_params:
            return queryset.filter(
                registration_id=request.query_params["registration_id"]
            )
        return queryset


class CertificateAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [
        CertificateRegistrationFilter,
        UserFilter,
    ]
    user_lookup_field = "registration__student"
    model_class = Certificate
    obj_user_groups = ["Admin", "Manager", "Accountant"]


class CertificatePublicAPI(RetrieveAPIView):
    lookup_url_kwarg = "uuid"
    lookup_field = "uuid"

    queryset = Certificate.objects.all()
    serializer_class = CertificatePublicSerializer
