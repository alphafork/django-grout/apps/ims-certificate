from django.conf import settings
from ims_base.serializers import BaseModelSerializer
from rest_framework.serializers import ModelSerializer

from ims_certificate.models import Certificate
from ims_certificate.services import get_certificate_data


class CertificateSerializer(BaseModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        certificate_url = f"{settings.FRONTEND_WEBSITE_URL}{settings.CERTIFICATE_QR_CODE_PATH}{instance.uuid}"
        return {**data, **get_certificate_data(instance, certificate_url)}


class CertificatePublicSerializer(ModelSerializer):
    class Meta:
        exclude = ["created_by", "updated_by", "created_at", "updated_at"]
        model = Certificate

    def to_representation(self, instance):
        data = super().to_representation(instance)
        certificate_url = f"{settings.FRONTEND_WEBSITE_URL}{settings.CERTIFICATE_QR_CODE_PATH}{instance.uuid}"
        return {**data, **get_certificate_data(instance, certificate_url)}
