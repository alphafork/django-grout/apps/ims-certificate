from django.apps import AppConfig


class IMSCertificateConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_certificate"

    model_strings = {
        "CERTIFICATE": "Certificate",
    }
