from ims_base.services import generate_qr_code_svg


def get_certificate_data(certificate, certificate_url):
    data = {}
    data["name"] = certificate.registration.student.user.get_full_name()
    url_qr = generate_qr_code_svg(certificate_url)
    data["url"] = certificate_url
    data["url_qr"] = url_qr
    return data
