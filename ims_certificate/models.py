import uuid

from django.db import models
from ims_base.models import AbstractLog
from reusable_models import get_model_from_string

StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")
BatchStudent = get_model_from_string("BATCH_STUDENT")


class Certificate(AbstractLog):
    serial_number = models.CharField(max_length=255, unique=True, null=True, blank=True)
    registration = models.OneToOneField(
        StudentRegistration, on_delete=models.CASCADE, primary_key=True
    )
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    date_of_issue = models.DateField(null=True, blank=True)
    program = models.CharField(max_length=255)
    is_downloadable = models.BooleanField(default=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.serial_number
