from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import CertificatePublicAPI

urlpatterns = [
    path("public/<uuid:uuid>/", CertificatePublicAPI.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
