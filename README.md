# IMS certificate

App to manage student certificates in dj-ims.


## License


  [AGPL-3.0-or-later](LICENSE)



## Contact

Alpha Fork Technologies

[connect@alphafork.com](mailto:connect@alphafork.com)
